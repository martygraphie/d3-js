# TITANIC PROJECT D3JS

## Context 
We have at our disposal the list of the passengers of the Titanic during its last voyage. The different columns, separated by tabs, indicate :
* the class of the passenger 
* if he survived or not 
* his name 
* his gender 
* his age 
* the fare he paid 

## Creating a scatter plot

The visualization will consist in displaying the two quantitative quantities (age and price) as the two x and y dimensions of a scatterplot, following Bertin's recommendations. We will display the different nominal values as follows: gender will be represented by a shape (square for men, round for women), the fact of having survived or not will be represented by transparency. The final result may look like the image below:

![Data Visualisation](titanic_project.png)
